package com.comtrade.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Brand {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String naziv;
	private String brandLogo;
	@OneToMany(mappedBy = "brand", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Artikal> setArtikal=new HashSet<Artikal>();
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getBrandLogo() {
		return brandLogo;
	}
	public void setBrandLogo(String brandLogo) {
		this.brandLogo = brandLogo;
	}
	public Set<Artikal> getSetArtikal() {
		return setArtikal;
	}
	public void setSetArtikal(Set<Artikal> setArtikal) {
		this.setArtikal = setArtikal;
	}
	
	public void addArtikal(Artikal artikal) {
		setArtikal.add(artikal);
		artikal.setBrand(this);
	}
	public void removeArtikal(Artikal artikal) {
		setArtikal.remove(artikal);
		artikal.setBrand(null);
	}
	
}
